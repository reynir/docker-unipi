#!/bin/sh -ex

if [ "$#" -lt 1 ]; then
	echo Must provide the unikernel as argument >&2
	exit 2
fi
unikernel=$1
shift
test -c /dev/net/tun || {
	echo Cannot access /dev/net/tun >&2
	exit 2
}
ip=$(ip a show eth0 | awk '/inet/{print $2}') || {
	echo Could not determine ip address >&2
	exit 2
}
gateway=$(ip route show default | awk '/^default /{print $3}') || {
	echo Could not determine default gateway >&2
	exit 2
}

ip tuntap add mode tap name service
ip addr del "$ip" dev eth0
ip link add br0 type bridge
ip link set eth0 master br0
ip link set service master br0
ip link set br0 up
ip link set service up
exec solo5-spt --net:service=service -- "$unikernel" --ipv4-only=true --ipv4="$ip" --ipv4-gateway="$gateway" "$@"
