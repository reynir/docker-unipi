FROM ocaml/opam:ubuntu-20.04-ocaml-4.14 AS opam
RUN sudo cp /usr/bin/opam-2.1 /usr/local/bin/opam
RUN sudo apt update && ( cd opam-repository && git pull origin master ) && opam update && \
    opam install 'mirage>4' && \
    eval $(opam env) && \
    git clone https://github.com/roburio/unipi.git && \
    ( cd unipi && mirage configure -t spt && make ) && \
    { bindir="$(opam var bin)"; cp $bindir/solo5-elftool $bindir/solo5-spt ~/; }

FROM ubuntu:20.04
COPY --from=opam /home/opam/unipi/dist/unipi.spt /unipi.spt
COPY --from=opam /home/opam/solo5-elftool /home/opam/solo5-spt /usr/local/bin/
COPY entrypoint.sh /entrypoint.sh
RUN apt update && apt install -y iproute2
EXPOSE 80
ENTRYPOINT ["/entrypoint.sh", "/unipi.spt"]
